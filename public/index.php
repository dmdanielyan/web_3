<?php

header('Content-Type: text/html; charset=UTF-8');

// В суперглобальном массиве $_SERVER PHP сохраняет некторые заголовки запроса HTTP
// и другие сведения о клиненте и сервере, например метод текущего запроса $_SERVER['REQUEST_METHOD'].
if ($_SERVER['REQUEST_METHOD'] == 'GET') {
  // В суперглобальном массиве $_GET PHP хранит все параметры, переданные в текущем запросе через URL.
  if (!empty($_GET['save'])) {
    // Если есть параметр save, то выводим сообщение пользователю.
    print('Спасибо, результаты сохранены.');
  }
  // Включаем содержимое файла form.php.
  include('form.php');
  // Завершаем работу скрипта.
  exit();
}
// Иначе, если запрос был методом POST, т.е. нужно проверить данные и сохранить их в XML-файл.


$ability_data = ['god', 'idclip', 'levitation'];

// Проверяем ошибки.
$errors = FALSE;
if (empty($_POST['fio'])) {
  print('Заполните имя.<br/>');
  $errors = TRUE;
}
else if (!preg_match('/^[а-яА-Я]+$/u', $_POST['fio'])) {
    print('Вы ввели несуществующие имя.<br/>');
    $errors = TRUE;
   
}
else if (!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) { 
    print('Вы ввели несуществующие e-mail.<br/>');
    $errors = TRUE;
}
else if (empty($_POST['year'])){
    print("Вы не выбрали год");
    $errors = TRUE;
}
else if (empty($_POST['sex'])){
    print("Вы не заполнили пол");
    $errors = TRUE;
}
else if (empty($_POST['abilities'])){
    print("Вы не заполнили суперспособность");
    $errors = TRUE;
}
else if (empty($_POST['bio'])) {
    print('Заполните биографию.<br/>');
    $errors = TRUE;
}
else if (empty($_POST['accept'])){
    print("Вы не приняли условия!");
    $errors = TRUE;
}
else {
    $abilities = $_POST['abilities'];
    foreach ($abilities as $ability){
        if (!in_array($ability, ['god', 'idclip', 'levitation'])){
            print("Вы выбрали не ту способность");
            $errors = TRUE;
        }
    }
}

$ability_insert = [];
$ability_labels = ['god' => 'Бессмертие', 
    'idclip' => 'прохождение сквозь стены', 
    'levitation'=>'левитация'];

foreach ($ability_data as $ability){
    $ability_insert[$ability] = in_array($ability, $abilities) ? 1 : 0;
}


// *************
// Тут необходимо проверить правильность заполнения всех остальных полей.
// *************

if ($errors) {
  // При наличии ошибок завершаем работу скрипта.
  exit();
}

// Сохранение в базу данных.

$user = 'u20382';
$pass = '6479868';
$db = new PDO('mysql:host=localhost;dbname=u20382', $user, $pass, array(PDO::ATTR_PERSISTENT => true));

// Подготовленный запрос. Не именованные метки.
try {
  $stmt = $db->prepare("INSERT INTO application SET name = ?, year = ?, sex = ?, ability_god = ?, ability_idclip = ?, ability_levitation = ?, biography = ?, accept = ? ");
  
  $stmt -> execute([$_POST['fio'], intval($_POST['year']),  $_POST['sex'], $ability_insert['god'], $ability_insert['idclip'], $ability_insert['levitation'], $_POST['bio'], $_POST['accept'] ]);
}
catch(PDOException $e){
  print('Error : ' . $e->getMessage()); 
  exit();
}

//  stmt - это "дескриптор состояния".
 
//  Именованные метки.
//$stmt = $db->prepare("INSERT INTO test (label,color) VALUES (:label,:color)");
//$stmt -> execute(array('label'=>'perfect', 'color'=>'green'));
 
//Еще вариант
/*$stmt = $db->prepare("INSERT INTO users (firstname, lastname, email) VALUES (:firstname, :lastname, :email)");
$stmt->bindParam(':firstname', $firstname);
$stmt->bindParam(':lastname', $lastname);
$stmt->bindParam(':email', $email);
$firstname = "John";
$lastname = "Smith";
$email = "john@test.com";
$stmt->execute();
*/

// Делаем перенаправление.
// Если запись не сохраняется, но ошибок не видно, то можно закомментировать эту строку чтобы увидеть ошибку.
// Если ошибок при этом не видно, то необходимо настроить параметр display_errors для PHP.
header('Location: ?save=1');
