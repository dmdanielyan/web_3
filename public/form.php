<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="generator" content="GitLab Pages">
		<title>task 3 of backend course by Sinitsa</title>
		<link rel="stylesheet" href="style.css">
	</head>
	<body>	
		<div class="center">
		
<form action="" method="POST">
  Ваше имя?<br>
  <input size='40' type='text' maxlength='25' name="fio" > <br>
  Ваш email?<br>
  <input size="60" name="email" type="text" maxlength="50"><br>
  Выберите год рождения?<br>
  <select name="year">
  	<?php for ($i = 1900; $i < 2020; $i++){ ?>
  		<option value=<?php print $i; ?>><?= $i; ?></option>
		<?php  } ?>  	
	</select><br>
	
	Выберите пол?<br>
					<label class="radio">
						<input type="radio" name="sex" value=0>
						Мужской
					</label>
					<label class="radio">
						<input type="radio" name="sex" value=1>
						Женский
					</label>
	

	Какие у вас сверхспособности?<br>
				<select multiple name="abilities[]">
					<option value="god">бессмертие</option>
					<option value="idclip">прохождение сквозь стены</option>
					<option value="levitation">левитация</option>
				</select><br>
				  
				  
	Ваша биография?<br>
				<textarea rows="10" cols="40" name="bio" ></textarea>
				
				С контрактом ознакомлен:
				<input type="checkbox" name="accept" value=1>Ознакомлен<br>
				
  <input type="submit" value="ok" />
  
</form>

		</div>
	</body>
</html>